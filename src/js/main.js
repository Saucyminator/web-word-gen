---
---

'use strict';

$(function() {
  var output    = [];
  var button   = $('#generate');
  var text     = $('#text');
  var maxWords = 30;
  var words    = [
    'Dator', 'Spelar', 'Kabel', 'Sladd',
    'Mamma', 'Mamman', 'Pappan', 'Pappa', 'Barnet', 'Barnen', 'Killen', 'Tjejen',
    'Broder', 'Brodern', 'Brorsa', 'Brorsan', 'Syster', 'Systern',
    'Stora', 'Lilla', 'Liten', 'Lillebror', 'Storebror', 'Lillasyster', 'Storasyster',
    'Flyga', 'Flygplan', 'Viking', 'Vikingar', 'Blomma', 'Hejsan', 'Menar', 'Heter',
    'Sover', 'Flicka', 'Pojke', 'Prata', 'Rolig', 'Roligt', 'Ledsen', 'Skulle',
    'Underbar', 'Finns', 'Hemma', 'Hemmet', 'Innan', 'Ingen', 'Julas', 'Julafton',
    'Efter', 'Resor', 'Alltid', 'Aldrig', 'Tillbaka', 'Sluta', 'Redan', 'Stycken', 'Kompis',
    'Gammal', 'Skicka', 'Ganska', 'Cykel', 'Nyckel', 'Genom', 'Goddag', 'Godnatt', 'Godmorgon',
    'Namnet', 'Namnen', 'Fickur', 'Korta', 'Kortare', 'Springa', 'Jogga', 'Hitta', 'Hittade',
    'Flytta', 'Titta', 'Kolla', 'Galen', 'Eftersom', 'Andra', 'Varit', 'Duktig', 'Duktigt', 'Duktiga',
    'Samma', 'Annorlunda', 'Morgon', 'Bilden', 'Bilder', 'Bilderna',
    'Godis', 'Godiset', 'Godisbit', 'Godisen', 'Gottegris',
    'Person', 'Personer', 'Personen', 'Ibland', 'Detta', 'Denna', 'Gillar',
    'Lycka', 'Lyckan', 'Lycklig', 'Pratar', 'Pratade', 'Pratsam', 'Musik', 'Livet', 'Liven',
    'Nyare', 'Svara', 'Svarat', 'Svarade', 'Trevligt', 'Trist', 'Finare', 'Finast', 'Rummet', 'Rummen',
    'Staden', 'Drack', 'Dricka', 'Druckit', 'Dricker', 'Drickan', 'Halva', 'Skatt', 'Skatten', 'Skatterna',
    'Skratt', 'Skrattet', 'Verkligen', 'Hoppa', 'Hoppas', 'Hoppar', 'Lugnet', 'Ugnen', 'Kunna', 'Kunnat',
    'Konstig', 'Konstigt', 'Konstiga', 'Mellan', 'Senare', 'Gjort', 'Gjorde', 'Blivit', 'Sjukhus', 'Sjukan', 'Sjukdom',
    'Stanna', 'Stannar', 'Stopp', 'Stoppa', 'Olika', 'Turen', 'Tursam', 'Tunnare', 'Tjock', 'Tjockare',
    'Dessutom', 'Leker', 'Nyfiken', 'Semester', 'Problem', 'Problemet', 'Problemen',
    'Betyder', 'Betydelse', 'Orkar', 'Orkat', 'Tycka', 'Tycker', 'Ligger', 'Trots', 'Lugnt',
    'Sommar', 'Vinter', 'Solen', 'Badet', 'Handduk',
    'Tacos', 'Potatis',
    'Tisdag', 'Onsdag', 'Torsdag', 'Fredag',
    'Svart', 'Svarta', 'Bruna',
    'Vatten', 'Vattenfall', 'Stenar', 'Floden', 'Floder',
    'Centimeter', 'Meter', 'Kilometer',
    'Armen', 'Armarna', 'Benet', 'Benen',
    'Tretton', 'Fjorton', 'Femton', 'Sexton', 'Sjutton', 'Arton', 'Nitton', 'Tjugo',
    'Bordet', 'Borden', 'Papper', 'Pappret', 'Boken', 'Ordbok', 'Orden', 'Skriva', 'Skrivit',
    'Djuret', 'Djuren', 'Hunden', 'Katten', 'Kossan', 'Pingvin', 'Grisen', 'Grisar', 'Aporna',
    'Sverige', 'Finland', 'Danmark', 'Tyskland', 'Frankrike',
    'Svenska', 'Finska', 'Danska', 'Tyska', 'Franska',
    'Bamse', 'Skalman', 'Pingo',
    'Fotboll', 'Bandy', 'Handboll', 'Domare',
    'Januari', 'Februari', 'April', 'Augusti', 'September', 'Oktober', 'November', 'December'
  ];

  /**
   * Returns a single random word from an array of words.
   *
   * @param  array list
   *
   * @return string
   */
  function randomWord (list) {
    return list[Math.floor(Math.random() * list.length)];
  };

  /**
   * Randomly generate three digits.
   *
   * @return string
   */
  function randomNumbers () {
    var val = Math.floor(Math.random() * 990);
    val += Math.floor((val+110)/110);

    return ('000' + val).substr(-3);
  };

  /**
   * Generates a list of words and numbers based on maximum words (maxWords).
   *
   * @return void
   */
  function generateList () {
    if (output.length > 0) {
      output = [];
    }

    while (output.length <= maxWords) {
      output.push(randomWord(words) + randomNumbers());
    }
  };

  button.on('click', function() {
    generateList();
    text.empty();

    $.each(output, function(){
      $(text).append(this + '<br>');
    });
  });
});
