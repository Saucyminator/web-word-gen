# Word generation
Generates Swedish words with at least five letters and three digits, created randomly (only one digit can occur maximum of two times).

## License
[MIT License](LICENSE.md)
